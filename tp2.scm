(define (tri_insert L comp)
  (if (null? L)
      ()
      (insere (car L) (tri_insert (cdr L) comp) comp)))

(define (insere)
  (lambda (x L comp)
    (if (null? L)
        (list x)
        (if (comp x (car L))
            (cons x L)
            (cons (car L) (insere x (cdr L) comp))))))

(tri_insert '(1 2 6 4 3) <)

(define compare_taille
  (lambda (a b)
; retourne vrai si a doit être avant b selon sa taille faux sinon
    (< (caddr a) (caddr b))))
(tri_insert '(("baba" 85 360)
              ("Anatole" 25 120)
              ("Philomène" 37 163))
            (lambda (a b)
              (string<?
               (car a)
               (car b))))

(define comp
  (lambda (f g)
    (lambda(x) (f (g x)))))

((comp (lambda (x) (+ x 2)) (lambda (x) (* x 3))) 2)

(define derivee
  (lambda (f)
    (let ((h 0.0001))
      (lambda (x) (/ (- (f (+ x h)) (f (- x h))) (* 2 h))))))

((derivee quad) 2)

(define quad
  (lambda (x)
    (* x (* x (* x x)))))

(quad 2)

    

(restart 1)
